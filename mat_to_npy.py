import scipy.io as sio
import numpy as np
import glob
import os
import pandas as pd


FEATURES_DIR = './ds/SkeletonData/'
LABELS_DIR = './ds/Annotate/'

skeleton_data_dir = glob.glob(FEATURES_DIR + '*.mat')

max_frames = 155
ds = np.empty((0, 155, 75))
labels = []
for skeleton_data_file in skeleton_data_dir:
    basename = os.path.basename(skeleton_data_file)
    name = os.path.splitext(basename)[0]
    print(name)

    #load features
    mat_contents = sio.loadmat(skeleton_data_file)
    skeleton_data = mat_contents['SkeletonData']
    print(skeleton_data.shape)

    #load labels
    labels_file_name = LABELS_DIR + name + '.xlsx'
    print(labels_file_name)
    df = pd.read_excel(labels_file_name)

    for index, row in df.iterrows():
        if pd.isnull(row[2]):
            continue

        clip = skeleton_data[int(row[0]-1):int(row[1]-1)]
        pad_rows = max_frames - clip.shape[0]
        clip = np.pad(clip, ((0, pad_rows), (0, 0)), mode='constant')
        clip = np.reshape(clip, (1, clip.shape[0], clip.shape[1]))
        ds = np.concatenate((ds, clip), axis=0)
        labels.append(int(row[2]))

print('ds shape', ds.shape)
print('max frames', max_frames)
np.save('full_ds.npy', ds)
np.save('full_ds_label.npy', np.array(labels))


#s_data = mat_contents['SkeletonData']
#
#
#labels = np.array([ 
#    0, 1, 17, 8, 15, 6, 9, 5, 
#    11, 16, 3, 18, 19, 16, 20,
#    13, 7, 12, 0, 2, 10, 0, 4, 0,
#    ])
#
#end_frame = np.array([
#    44, 99, 146, 186, 238,
#    311, 349, 388, 434, 462,
#    508, 548, 575, 617, 669, 714,
#    763, 797, 830, 870, 895, 921,
#    974, 987, 
#    ])
#
#start_frame = np.array([
#    1, 45, 100, 147, 187,
#    239, 312, 350, 389, 435,
#    463, 509, 549, 576, 618,
#    670, 715, 764, 798, 831,
#    871, 896, 922, 975,
#    ])
#
#leng = np.subtract(end_frame, start_frame)
#
#max_rows = np.max(leng)
#print(max_rows)
#
#ds = np.empty((0, 72, 75))
#
#for idx, sf in enumerate(start_frame):
#    ef = end_frame[idx]
#    clip = s_data[sf-1:ef-1]
#    pad_rows = max_rows - clip.shape[0]
#    clip = np.pad(clip, ((0, pad_rows), (0, 0)), mode='constant')
#    clip = np.reshape(clip, (1, clip.shape[0], clip.shape[1]))
#
#    ds = np.concatenate((ds, clip), axis=0)
#
#np.save('test001.npy', ds)
#np.save('test001_label.npy', labels)
#np.save('test001_len.npy', leng)
#
