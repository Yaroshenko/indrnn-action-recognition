"""Module using IndRNNCell to solve the sequential MNIST task.
The hyper-parameters are taken from that paper as well.

"""
from IndRNN_onlyrecurrent import IndRNN_onlyrecurrent
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as utils
from torchvision import datasets, transforms
import numpy as np
import argparse
from time import time
from torch.utils.data.sampler import SubsetRandomSampler
import Indrnn_action_network
import opts     

OUTPUT_CLASSES = 21
INPUT_FEATURES = 75

parser = argparse.ArgumentParser(description='pytorch action')
opts.train_opts(parser)
args = parser.parse_args()

args.cuda = not args.no_cuda and torch.cuda.is_available()
cuda = torch.cuda.is_available()



def main():
    model = Indrnn_action_network.stackedIndRNN_encoder(INPUT_FEATURES, OUTPUT_CLASSES)  
    if cuda:
        model.cuda()
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)

    # load data
    train_data, test_data = sequential(args.batch_size, cuda=cuda)

    # Train the model
    model.train()
    step = 0
    epochs = 0
    while step < args.max_steps:
        losses = []
        start = time()
        for data, target in train_data:
            if cuda:
                data, target = data.cuda(), target.cuda()
            model.zero_grad()
            out = model(data)
            loss = F.cross_entropy(out, target.squeeze_())
            loss.backward()
            optimizer.step()
            losses.append(loss.data.cpu().item())
            step += 1

            if step % args.log_iteration == 0 and args.log_iteration != -1:
                print("\tStep {} cross_entropy {:.4f}".format( step, np.mean(losses)))
            if step >= args.max_steps:
                break
        if epochs % args.log_epoch == 0:
            print(
                    "Epoch {} cross_entropy {:.4f} ({:.2f} sec.)".format(
                    epochs, np.mean(losses), time()-start))
            eval(model, test_data)
        epochs += 1




def eval(model, test_data):
    # get test error
    model.eval()
    correct = 0
    for data, target in test_data:
        if cuda:
            data, target = data.cuda(), target.cuda()
        out = model(data)
        pred = out.data.max(1, keepdim=True)[1]
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()
    print('Correct', correct, 'len', len(test_data.dataset))
    acc = 100. * float(correct) / len(test_data.dataset)
    print("Test acc {:.2f} %".format(acc))


def sequential(batch_size, cuda=False, dataset_folder='./data'):

    sk_x = np.load('../full_ds.npy')
    sk_y = np.load('../full_ds_label.npy')
    sk_y = sk_y.reshape(-1, 1)

    tensor_x = torch.stack([torch.Tensor(i) for i in sk_x])
    tensor_y = torch.stack([torch.from_numpy(i).long() for i in sk_y])

    full_dataset = utils.TensorDataset(tensor_x,tensor_y)

    train_size = int(0.8 * len(full_dataset))
    test_size = len(full_dataset) - train_size
    train_dataset, test_dataset = torch.utils.data.random_split(full_dataset, [train_size, test_size])

    kwargs = {'num_workers': 1, 'pin_memory': True} if cuda else {}

    sk_dl_train = utils.DataLoader(
            train_dataset, 
            batch_size=batch_size, 
            shuffle=True, 
            drop_last=True, 
            )

    sk_dl_test = utils.DataLoader(
            test_dataset, 
            batch_size=batch_size, 
            shuffle=False, 
            drop_last=True, 
            )
    return (sk_dl_train, sk_dl_test)


if __name__ == "__main__":
    main()
