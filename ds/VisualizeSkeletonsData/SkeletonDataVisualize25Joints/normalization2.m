function [position_data_new] =  normalization2(pose_data)
    
    hip_center = pose_data(1:3);
    dspine = compute_distance_between_two_joints(pose_data(4:6),pose_data(7:9));
   
    new_pos1 = compute_new_position(pose_data(1:3),hip_center,dspine);
    new_pos2 = compute_new_position(pose_data(4:6),hip_center,dspine);
    new_pos3 = compute_new_position(pose_data(7:9),hip_center,dspine);
    new_pos4 = compute_new_position(pose_data(10:12),hip_center,dspine);
    
    new_pos5 = compute_new_position(pose_data(13:15),hip_center,dspine);
    new_pos6 = compute_new_position(pose_data(16:18),hip_center,dspine);
    new_pos7 = compute_new_position(pose_data(19:21),hip_center,dspine);
    new_pos8 = compute_new_position(pose_data(22:24),hip_center,dspine);
    
    new_pos9 = compute_new_position(pose_data(25:27),hip_center,dspine);
    new_pos10 = compute_new_position(pose_data(28:30),hip_center,dspine);
    new_pos11 = compute_new_position(pose_data(31:33),hip_center,dspine);
    new_pos12 = compute_new_position(pose_data(34:36),hip_center,dspine);
    
    
    new_pos13 = compute_new_position(pose_data(37:39),hip_center,dspine);
    new_pos14 = compute_new_position(pose_data(40:42),hip_center,dspine);
    new_pos15 = compute_new_position(pose_data(43:45),hip_center,dspine);
    new_pos16 = compute_new_position(pose_data(46:48),hip_center,dspine);
    
    new_pos17 = compute_new_position(pose_data(49:51),hip_center,dspine);
    new_pos18 = compute_new_position(pose_data(52:54),hip_center,dspine);
    new_pos19 = compute_new_position(pose_data(55:57),hip_center,dspine);
    new_pos20 = compute_new_position(pose_data(58:60),hip_center,dspine);
    new_pos21 = compute_new_position(pose_data(61:63),hip_center,dspine);
    new_pos22 = compute_new_position(pose_data(64:66),hip_center,dspine);
    new_pos23 = compute_new_position(pose_data(67:69),hip_center,dspine);
    new_pos24 = compute_new_position(pose_data(70:72),hip_center,dspine);
    new_pos25 = compute_new_position(pose_data(73:75),hip_center,dspine);
    
    
%     position_data_new = [pose_data(:,10:12),new_pos34,new_pos53,new_pos65,new_pos76,new_pos87,new_pos93,new_pos109,new_pos1110,...
%         new_pos1211,new_pos23,new_pos12,new_pos131,new_pos1413,new_pos1514,new_pos1615,new_pos171,new_pos1817,new_pos1918,new_pos2019];
        
     position_data_new = [new_pos1,new_pos2,new_pos3,new_pos4,new_pos5,new_pos6,new_pos7,new_pos8,new_pos9,...
        new_pos10,new_pos11,new_pos12,new_pos13,new_pos14,new_pos15,new_pos16,new_pos17,new_pos18,new_pos19,new_pos20,...
        new_pos21,new_pos22,new_pos23,new_pos24,new_pos25];
end


function [new_position] = compute_new_position(v,ref,distance)
       position = v - ref;
       new_position = position./distance;
end


function [distance] = compute_distance_between_two_joints(v1,v2)
    v = (v1-v2).^2;
    distance = sqrt(sum(v));

end