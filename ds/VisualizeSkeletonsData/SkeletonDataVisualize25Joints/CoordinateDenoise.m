function [ SmoothedData ] = CoordinateDenoise(DATA)
%DATA: Input Original Coordinates x1 y1 z1 x2 y2 z2 x3 y3 z3......x20 y20 z20
SequenceLength=size(DATA,1);
SmoothedData=zeros(size(DATA));
SmoothedData(1,:)=DATA(1,:);
SmoothedData(SequenceLength,:)=DATA(SequenceLength,:);
for i=2:SequenceLength-1
    SmoothedData(i,:)=(1/3)*(DATA(i,:)+DATA(i-1,:)+DATA(i+1,:));
end

end

