
clear all;
clc;
disp('Visualizing sequence...');
[X]=load('\\piggie.cs.uow.edu.au\chang\ChangTang\SkeletonData\OnlineAction3D\SegmentedTrainningData\a18_s15_e02_skeleton.mat');
SkeletonData=X.Skeleton;
T=size(SkeletonData,1);	% Length of sequence in frames

% Animate sequence
h=axes;
% DATA = SkeletonData;
DATA=[];
for ti=1:T
    YData = SkeletonData(ti,:);
    [data_new] = normalization2(YData);
    DATA = [DATA;data_new];
end
DATA=CoordinateDenoise(DATA);

for ti=1:T
    skel_vis(DATA,ti,h); 
    drawnow;
    
    pause(1/60);
    cla;
end

