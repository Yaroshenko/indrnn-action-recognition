function [position_data_new] =  normalization(pose_data)
 
    distance34 = compute_distance_between_two_joints(pose_data(:,7:9),pose_data(:,10:12));
    distance53 = compute_distance_between_two_joints(pose_data(:,13:15),pose_data(:,7:9));
    distance65 = compute_distance_between_two_joints(pose_data(:,16:18),pose_data(:,13:15));
    distance76 = compute_distance_between_two_joints(pose_data(:,19:21),pose_data(:,16:18));
    distance87 = compute_distance_between_two_joints(pose_data(:,22:24),pose_data(:,19:21));
    distance93 = compute_distance_between_two_joints(pose_data(:,25:27),pose_data(:,7:9));
    distance109 = compute_distance_between_two_joints(pose_data(:,28:30),pose_data(:,25:27));
    distance1110 = compute_distance_between_two_joints(pose_data(:,31:33),pose_data(:,28:30));
    distance1211 = compute_distance_between_two_joints(pose_data(:,34:36),pose_data(:,31:33));
    distance23 = compute_distance_between_two_joints(pose_data(:,4:6),pose_data(:,7:9));
    distance12 = compute_distance_between_two_joints(pose_data(:,1:3),pose_data(:,4:6));
    distance131 = compute_distance_between_two_joints(pose_data(:,37:39),pose_data(:,1:3));
    distance1413 = compute_distance_between_two_joints(pose_data(:,40:42),pose_data(:,37:39));
    distance1514 = compute_distance_between_two_joints(pose_data(:,43:45),pose_data(:,40:42));
    distance1615 = compute_distance_between_two_joints(pose_data(:,46:48),pose_data(:,43:45));
    distance171 = compute_distance_between_two_joints(pose_data(:,49:51),pose_data(:,1:3));
    distance1817 = compute_distance_between_two_joints(pose_data(:,52:54),pose_data(:,49:51));
    distance1918 = compute_distance_between_two_joints(pose_data(:,55:57),pose_data(:,52:54));
    distance2019 = compute_distance_between_two_joints(pose_data(:,58:60),pose_data(:,55:57));
    
    mean_distance = (distance34 + distance53 + distance65 + distance76 + distance87 + distance93 + distance109 ...
                     + distance1110 +  distance1211 +  distance23 +  distance12 + distance131 + distance1413 + distance1514 ...
                     + distance1615 + distance171 + distance1817 +distance1918 +distance2019)/19;
   
    new_pos21 = compute_new_position(pose_data(:,1:3),pose_data(:,4:6),pose_data(:,1:3),mean_distance);
    new_pos32 = compute_new_position(new_pos21,pose_data(:,7:9),pose_data(:,4:6),mean_distance);
    new_pos43 = compute_new_position(new_pos32,pose_data(:,10:12),pose_data(:,7:9),mean_distance);
    
    new_pos53 = compute_new_position(new_pos32,pose_data(:,13:15),pose_data(:,7:9),mean_distance);
    new_pos65 = compute_new_position(new_pos53,pose_data(:,16:18),pose_data(:,13:15),mean_distance);
    new_pos76 = compute_new_position(new_pos65,pose_data(:,19:21),pose_data(:,16:18),mean_distance);
    new_pos87 = compute_new_position(new_pos76,pose_data(:,22:24),pose_data(:,19:21),mean_distance);
    
    new_pos93 = compute_new_position(new_pos32,pose_data(:,25:27),pose_data(:,7:9),mean_distance);
    new_pos109 = compute_new_position(new_pos93,pose_data(:,28:30),pose_data(:,25:27),mean_distance);
    new_pos1110 = compute_new_position(new_pos109,pose_data(:,31:33),pose_data(:,28:30),mean_distance);
    new_pos1211 = compute_new_position(new_pos1110,pose_data(:,34:36),pose_data(:,31:33),mean_distance);
    
    
    new_pos131 = compute_new_position(pose_data(:,1:3),pose_data(:,37:39),pose_data(:,1:3),mean_distance);
    new_pos1413 = compute_new_position(new_pos131,pose_data(:,40:42),pose_data(:,37:39),mean_distance);
    new_pos1514 = compute_new_position(new_pos1413,pose_data(:,43:45),pose_data(:,40:42),mean_distance);
    new_pos1615 = compute_new_position(new_pos1514,pose_data(:,46:48),pose_data(:,43:45),mean_distance);
    
    new_pos171 = compute_new_position(pose_data(:,1:3),pose_data(:,49:51),pose_data(:,1:3),mean_distance);
    new_pos1817 = compute_new_position(new_pos171,pose_data(:,52:54),pose_data(:,49:51),mean_distance);
    new_pos1918 = compute_new_position(new_pos1817,pose_data(:,55:57),pose_data(:,52:54),mean_distance);
    new_pos2019 = compute_new_position(new_pos1918,pose_data(:,58:60),pose_data(:,55:57),mean_distance);
    
    
%     position_data_new = [pose_data(:,10:12),new_pos34,new_pos53,new_pos65,new_pos76,new_pos87,new_pos93,new_pos109,new_pos1110,...
%         new_pos1211,new_pos23,new_pos12,new_pos131,new_pos1413,new_pos1514,new_pos1615,new_pos171,new_pos1817,new_pos1918,new_pos2019];
        
     position_data_new = [pose_data(:,1:3),new_pos21,new_pos32,new_pos43,new_pos53,new_pos65,new_pos76,new_pos87,new_pos93,...
        new_pos109,new_pos1110,new_pos1211,new_pos131,new_pos1413,new_pos1514,new_pos1615,new_pos171,new_pos1817,new_pos1918,new_pos2019];
end


function [new_position] = compute_new_position(v3,v1,v2,mean_distance)
       new_position = v3 + (v1-v2)/compute_distance_between_two_joints(v1,v2)*mean_distance;
end


function [distance] = compute_distance_between_two_joints(v1,v2)
    v = (v1-v2).^2;
    distance = sqrt(sum(v));

end