
clear all;
clc;
disp('Visualizing sequence...');
[X]=load('C:\Users\ct505\Documents\ProgramRunning\ActionRecognition\OnlineAction3D\HPC\Data\SegmentedTrainningData\a13_s06_e02_skeleton.mat');
SkeletonData=X.Skeleton;
T=size(SkeletonData,1);	% Length of sequence in frames

% Animate sequence
h=axes;
% DATA = SkeletonData;
DATA=[];
SkeletonData=SkeletonData(:,1:60);
for ti=1:T
    YData = SkeletonData(ti,:);
    [data_new] = normalization2(YData);
    DATA = [DATA;data_new];
end
DATA=CoordinateDenoise(DATA);

for ti=1:T
    skel_vis(DATA,ti,h); 
    drawnow;
    
    pause(1/60);
    cla;
end

